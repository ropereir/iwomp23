/**
 * Compile with
 *  - mpc_cc -O0 -g -Wall -Wextra -Werror -fopenmp deadlock.c
 */

# include <omp.h>
# include <stdio.h>
# ifdef MPC
#  include <mpc_omp.h>
#  define PREPARE_TASK(L)   do {                        \
                                mpc_omp_task_fiber();   \
                                mpc_omp_task_label(L);  \
                            } while (0)
# else /* MPC */
#  define PREPARE_TASK(L)
# endif /* MPC */

static volatile int T1_completed = 0;
static volatile int T2_started = 0;

static void
T1(void)
{
    puts("T1 start and yield");
    while (!T2_started)
    {
        # pragma omp taskyield
    }
    puts("T1 resumed");
    T1_completed = 1;
}

static void
T2(void)
{
    puts("T2 start");
    T2_started = 1;
    while (!T1_completed)
    {
        puts("T2 yield");
        # pragma omp taskyield
        puts("T2 resume");
    }
    puts("T2 completed");
}

int
main(void)
{
    puts("Starting...");
    # pragma omp parallel
    {
        while (!T1_completed && omp_get_thread_num() != 0);

        # pragma omp single
        {
            PREPARE_TASK("T2");
            # pragma omp task untied
            {
                T2();
            }

            PREPARE_TASK("T1");
            # pragma omp task untied
            {
                T1();
            }
        }
    }
    puts("Success");
    return 0;
}

