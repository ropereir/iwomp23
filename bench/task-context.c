// build: mpc_cc -cc=clang -O3 -Wall -Wextra -fopenmp context.c -o a.out.mpc -DCSV

#include <assert.h>
#include <omp.h>
#include <stdio.h>
#include <unistd.h>
#include <stdatomic.h>

static int N = 0;
static atomic_int j = 0;

static void
task(void)
{
    j = 0;

    for (int i = 0; i < N; ++i)
    {
        #pragma omp task default(none) shared(j)
        {
            ++j;
        }
    }

    #pragma omp taskwait
    assert(j == N);
}

static void
task_context(void)
{
    j = 0;

    for (int i = 0; i < N; ++i)
    {
        #pragma omp task default(none) shared(j) context
        {
            ++j;
        }
    }

    #pragma omp taskwait
    assert(j == N);
}

static void
warmup(void)
{
    task();
}

int
main(int argc, char ** argv)
{
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s [N]\n", argv[0]);
        return 1;
    }
    N = atoi(argv[1]);

    #pragma omp parallel
    {
        #pragma omp single
        {
#ifndef CSV
            const double t0 = omp_get_wtime();
#endif
            warmup();
            const double t1 = omp_get_wtime();
            task();
            const double t2 = omp_get_wtime();
            task_context();
            const double t3 = omp_get_wtime();

#ifdef CSV
            printf("threads:task:task_context\n");
            printf("%d:%lf:%lf\n", omp_get_num_threads(), t2 - t1, t3 - t2);
#else
            printf("warmup took %lf s.\n", t1 - t0);
            printf("task took %lf s.\n", t2 - t1);
            printf("task context took %lf s.\n", t3 - t2);
#endif /* CSV */
        }
    }
    return 0;
}
