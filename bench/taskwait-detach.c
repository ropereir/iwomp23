// build: mpc_cc -cc=clang -O3 -Wall -Wextra -fopenmp detach.c -o a.out.mpc -DCSV

#include <assert.h>
#include <omp.h>
#include <stdio.h>
#include <unistd.h>
#include <stdatomic.h>

static int N = 0;
static char * deps = {0};
static atomic_int j = 0;

static void
task(void)
{
    omp_event_handle_t hdl;
    j = 0;

    for (int i = 0; i < N; ++i)
    {
        #pragma omp task shared(j) depend(out: deps[i]) detach(hdl)
        {
            ++j;
        }

        #pragma omp task shared(j) depend(in: deps[i])
        {
            ++j;
        }

        #pragma omp task firstprivate(hdl)
        {
            omp_fulfill_event(hdl);
        }
    }

    #pragma omp taskwait
    assert(j == 2 * N);
}

static void
taskwait(void)
{
    j = 0;
    for (int i = 0; i < N; ++i)
    {
        #pragma omp task default(none) firstprivate(i) shared(j)
        {
            omp_event_handle_t hdl = omp_task_continuation_event();

            #pragma omp task default(none) firstprivate(hdl)
            {
                omp_fulfill_event(hdl);
            }

            ++j;
            #pragma omp taskwait detach(hdl)
            ++j;
        }
    }
    #pragma omp taskwait
    assert(j == 2 * N);
}

# if 0
static void
taskwait(void)
{
    j = 0;
    for (int i = 0; i < N; ++i)
    {
        #pragma omp task default(none) firstprivate(i) shared(j)
        {
            // omp_event_handle_t hdl = omp_task_continuation_event();
            #pragma omp task default(none) // firstprivate(hdl)
            {
                // omp_fulfill_event(hdl);
            }
            ++j;
            #pragma omp taskwait // detach(hdl)
            ++j;
        }
    }
    #pragma omp taskwait
    assert(j == 2 * N);
}
#endif

static void
warmup(void)
{
    task();
}

int
main(int argc, char ** argv)
{
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s [N]\n", argv[0]);
        return 1;
    }
    N = atoi(argv[1]);

    #pragma omp parallel
    {
        usleep(1000);
        #pragma omp single
        {
            deps = (char *) malloc(N);

#ifndef CSV
            const double t0 = omp_get_wtime();
#endif
            warmup();
            const double t1 = omp_get_wtime();
            task();
            const double t2 = omp_get_wtime();
            taskwait();
            const double t3 = omp_get_wtime();

#ifdef CSV
            printf("threads:task:taskwait\n");
            printf("%d:%lf:%lf\n", omp_get_num_threads(), t2 - t1, t3 - t2);
#else
            printf("warmup took %lf s.\n", t1 - t0);
            printf("task detach took %lf s.\n", t2 - t1);
            printf("taskwait detach took %lf s.\n", t3 - t2);
#endif /* CSV */
            free(deps);
        }
        usleep(1000);
    }
    return 0;
}
