#ifndef __FIBER_H__
# define __FIBER_H__

/* TODO : add preprocessor to include right files */
# include <arch/linux/x86/fiber.h>
# include <stdint.h>

typedef enum
{
    FIBER_SUCCESS  = 0,
    FIBER_ERROR    = 1
} fiber_errcode_t;

typedef struct
{
    void * ptr;
    unsigned int size;
} fiber_stack_t;

/* registers */
typedef struct
{
    /* ... */
    uintptr_t values[NREGS];
} fiber_regs_t;

typedef struct fiber_s
{
    void            (*entry);
    fiber_stack_t   stack;
    fiber_regs_t    regs;
} fiber_t;

fiber_errcode_t fiber_get(fiber_t *);
fiber_errcode_t fiber_set(fiber_t *);
fiber_errcode_t fiber_dup(fiber_t *, fiber_t *);
fiber_errcode_t fiber_make(fiber_t *);
fiber_errcode_t fiber_swap(fiber_t *, fiber_t *);

#endif /* __FIBER_H__ */
