#include <fiber.h>
#include <stdio.h>

static fiber_t f1, f2;
static int n = 0;

static void
fiber_main(void)
{
    printf("Inside fiber_main: n=%d\n", n);
    ++n;
    printf("Exiting fiber_main: n=%d\n", n);
    fiber_set(&f2);
}

int
main(void)
{
    char stack[8192];
    f1.entry        = fiber_main;
    f1.stack.ptr    = (void *) stack;
    f1.stack.size   = sizeof(stack);

    if (fiber_make(&f1) != FIBER_SUCCESS)
    {
        fprintf(stderr, "Error making fiber\n");
        return -1;
    }

    if (fiber_swap(&f2, &f1) != FIBER_SUCCESS)
    {
        fprintf(stderr, "Error swapping to fiber\n");
        return -1;
    }

    printf("Returned to main\n");

    return 0;
}
