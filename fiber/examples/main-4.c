#include <fiber.h>
#include <stdio.h>
#include <stdlib.h>

static fiber_t f1, f2;
static int run = 0;

static void
fiber_main(void)
{
    printf("Inside fiber_main\n");
    run = 1;
    printf("Exiting fiber_main\n");
    if (fiber_set(&f2) != FIBER_SUCCESS)
    {
        fprintf(stderr, "Error setting fiber f2\n");
        exit(-1);
    }
}

void
dump(char * label, fiber_t * f)
{
    printf("%s fiber %p\n", label, f);
    printf("  ->stack\n");
    printf("    .ptr  = %p\n", f->stack.ptr);
    printf("    .size = %u\n", f->stack.size);
    printf("  ->regs\n");
    printf("    .values\n");
    for (int i = 0 ; i < NREGS ; ++i)
    {
        if (i % 2 == 0 && i != 0) printf("\n");
        printf("%s [%2d] = %16lu", i % 2 == 0 ? "      " : ",", i, f->regs.values[i]);
    }
    if (NREGS % 2 != 0) printf("\n");
}

int
main(void)
{
    char stack[8192];

    f1.entry        = fiber_main;
    f1.exit         = NULL;
    f1.stack.ptr    = (void *) stack;
    f1.stack.size   = sizeof(stack);

    if (fiber_make(&f1) != FIBER_SUCCESS)
    {
        fprintf(stderr, "Error making fiber\n");
        return -1;
    }
    //dump("f1", &f1);

    if (fiber_get(&f2) != FIBER_SUCCESS)
    {
        fprintf(stderr, "Error getting fiber f2\n");
        return -1;
    }
    //dump("f2", &f2);

    if (run == 0)
    {
        if (fiber_set(&f1) != FIBER_SUCCESS)
        {
            fprintf(stderr, "Error setting fiber f1\n");
            return -1;
        }
    }
    //dump("f2", &f1);

    printf("Returned to main\n");

    return 0;
}
