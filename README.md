# Install MPC
```
# build
tar -xf mpcframework-4.1.0.tar.gz
export MPC=$(realpath mpcframework-4.1.0/)
mkdir $MPC/build
mkdir $MPC/install
cd $MPC/build
$MPC/installmpc --prefix=$MPC/install --process-mode --disable-mpc-comp --mpcframework="--enable-openmp --disable-lowcomm --disable-mpi"

# setup environment
source $MPC/install/mpcvars.sh
```

# Apply patch to LLVM
```
git clone --branch release/16.x https://github.com/llvm/llvm-project.git
cd llvm-project
patch -p0 < llvm-patch.diff
# now, you have to build LLVM Clang
```

# Compile a program
```
# with MPC-OMP runtime
mpc_cc -cc=clang -Wall -Weror -Wextra -fopenmp main.c

# with LLVM-OMP runtime
clang -Wall -Weror -Wextra -fopenmp main.c
```

# Note to the MPC team
commit 048ba32ab
